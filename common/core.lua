local core = {
    
    -- @description Take threats from SQF and convert them to lua valid threats
    -- @argument sqfThreats [table]
    -- example_fr_core.ProcessSQFThreats(sqfThreats)
    -- ProcessSQFThreats(sqfThreats) -- only in this btset
    ["ProcessSQFThreats"] = function(sqfThreats)
        local threats = {}

        for i=1, #sqfThreats do
            local thisThreat = sqfThreats[i]

            -- Local threat
            if thisThreat.position ~= nil and thisThreat.radius ~= nil then  
                threats[i] = {
                   area = Circle(thisThreat.position, thisThreat.radius),
                   avoidance = thisThreat.avoidance
                }
            -- Global threat 
            else
                threats[i] = {
                    avoidance = thisThreat.avoidance
                }
            end
        end
        return threats
    end,

    --[[
    ["mathExtra"] = {
        ["ProcessSQFThreats"] = function(sqfThreats)
        
        end,
    },
    ]]--
}

return core